#!/usr/bin/env bash
# Dependencies: bash>=3.2, coreutils, file

# Makes the script more portable
readonly DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Optional icon to display before the text
# Insert the absolute path of the icon
# Recommended size is 24x24 px
declare -r ICON_ARRAY=(
  "${DIR}/icons/gifs/anim-1-rz.gif"
  "${DIR}/icons/gifs/anim-2-rz.gif"
  "${DIR}/icons/gifs/anim-3-rz.gif"
  "${DIR}/icons/gifs/anim-4-rz.gif"
  "${DIR}/icons/gifs/anim-5-rz.gif"
  "${DIR}/icons/gifs/anim-19-rz.gif"
  "${DIR}/icons/gifs/anim-7-rz.gif"
  "${DIR}/icons/gifs/anim-8-rz.gif"
  "${DIR}/icons/gifs/anim-9-rz.gif"
  "${DIR}/icons/gifs/anim-10-rz.gif"
  "${DIR}/icons/gifs/anim-11-rz.gif"
  "${DIR}/icons/gifs/anim-12-rz.gif"
  "${DIR}/icons/gifs/anim-13-rz.gif"
  "${DIR}/icons/gifs/anim-14-rz.gif"
  "${DIR}/icons/gifs/anim-15-rz.gif"
  "${DIR}/icons/gifs/anim-16-rz.gif"
  "${DIR}/icons/gifs/anim-17-rz.gif"
  "${DIR}/icons/gifs/anim-19-rz.gif"
  "${DIR}/icons/gifs/anim-19-rz.gif"
  "${DIR}/icons/gifs/anim-20-rz.gif"
  "${DIR}/icons/gifs/anim-21-rz.gif"
  "${DIR}/icons/gifs/anim-22-rz.gif"
  "${DIR}/icons/gifs/anim-23-rz.gif"
  "${DIR}/icons/gifs/anim-24-rz.gif"
  "${DIR}/icons/gifs/anim-25-rz.gif"
  "${DIR}/icons/gifs/anim-26-rz.gif"
  "${DIR}/icons/gifs/anim-27-rz.gif"
  "${DIR}/icons/gifs/anim-28-rz.gif"
  "${DIR}/icons/gifs/anim-29-rz.gif"
  "${DIR}/icons/gifs/anim-30-rz.gif"
  "${DIR}/icons/gifs/anim-31-rz.gif"
  "${DIR}/icons/gifs/anim-32-rz.gif"
  "${DIR}/icons/gifs/anim-33-rz.gif"
  "${DIR}/icons/gifs/anim-34-rz.gif"
  "${DIR}/icons/gifs/anim-35-rz.gif"
  "${DIR}/icons/gifs/anim-36-rz.gif"
  "${DIR}/icons/gifs/anim-37-rz.gif"
  "${DIR}/icons/gifs/anim-38-rz.gif"
  "${DIR}/icons/gifs/anim-39-rz.gif"
  "${DIR}/icons/gifs/anim-40-rz.gif"
  "${DIR}/icons/gifs/anim-41-rz.gif"
  "${DIR}/icons/gifs/anim-42-rz.gif"
  "${DIR}/icons/gifs/anim-43-rz.gif"
  "${DIR}/icons/gifs/anim-44-rz.gif"
  "${DIR}/icons/gifs/anim-45-rz.gif"
  "${DIR}/icons/gifs/anim-46-rz.gif"
  "${DIR}/icons/gifs/anim-47-rz.gif"
  "${DIR}/icons/gifs/anim-48-rz.gif"
  "${DIR}/icons/gifs/anim-49-rz.gif"
  "${DIR}/icons/gifs/anim-50-rz.gif"
  "${DIR}/icons/gifs/anim-51-rz.gif"
  "${DIR}/icons/gifs/anim-52-rz.gif"
  "${DIR}/icons/gifs/anim-53-rz.gif"
  "${DIR}/icons/gifs/anim-54-rz.gif"
  "${DIR}/icons/gifs/anim-55-rz.gif"
  "${DIR}/icons/gifs/anim-56-rz.gif"
  "${DIR}/icons/gifs/anim-57-rz.gif"
  "${DIR}/icons/gifs/anim-58-rz.gif"
  "${DIR}/icons/gifs/anim-59-rz.gif"
  "${DIR}/icons/gifs/anim-60-rz.gif"
  "${DIR}/icons/gifs/anim-61-rz.gif"
  "${DIR}/icons/gifs/anim-62-rz.gif"
  "${DIR}/icons/gifs/anim-63-rz.gif"
  "${DIR}/icons/gifs/anim-64-rz.gif"
  "${DIR}/icons/gifs/anim-65-rz.gif"
  "${DIR}/icons/gifs/anim-66-rz.gif"
  "${DIR}/icons/gifs/anim-67-rz.gif"
  "${DIR}/icons/gifs/anim-68-rz.gif"
  "${DIR}/icons/gifs/anim-69-rz.gif"
  "${DIR}/icons/gifs/anim-70-rz.gif"
  "${DIR}/icons/gifs/anim-71-rz.gif"
  "${DIR}/icons/gifs/anim-72-rz.gif"
  "${DIR}/icons/gifs/anim-73-rz.gif"
  "${DIR}/icons/gifs/anim-74-rz.gif"
  "${DIR}/icons/gifs/anim-75-rz.gif"
  "${DIR}/icons/gifs/anim-76-rz.gif"
  "${DIR}/icons/gifs/anim-77-rz.gif"
  "${DIR}/icons/gifs/anim-19-rz.gif"
  "${DIR}/icons/gifs/anim-79-rz.gif"
  "${DIR}/icons/gifs/anim-80-rz.gif"
  "${DIR}/icons/gifs/anim-80-rz.gif"
  "${DIR}/icons/gifs/anim-81-rz.gif"
  "${DIR}/icons/gifs/anim-82-rz.gif"
  "${DIR}/icons/gifs/anim-83-rz.gif"
  "${DIR}/icons/gifs/anim-84-rz.gif"
  "${DIR}/icons/gifs/anim-85-rz.gif"
  "${DIR}/icons/gifs/anim-86-rz.gif"
  "${DIR}/icons/gifs/anim-87-rz.gif"
  "${DIR}/icons/gifs/anim-88-rz.gif"
  "${DIR}/icons/gifs/anim-89-rz.gif"
  "${DIR}/icons/gifs/anim-90-rz.gif"
  
)

# Compute random die
DIE=$(( RANDOM % 90 ))

# Panel
if [[ $(file -b "${ICON_ARRAY[DIE]}") =~ GIF|PNG ]]; then
  INFO="<img>${ICON_ARRAY[DIE]}</img>"
  
fi

# Tooltip
MORE_INFO="<tool>"
MORE_INFO+="$(( DIE + 1 ))"
MORE_INFO+="</tool>"

# Panel Print
echo -e "${INFO}"

# Tooltip Print
echo -e "${MORE_INFO}"
